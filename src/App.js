import { Component } from "react";
import {
  Container,
  Form,
  FormControl,
  Nav,
  Navbar,
  Button,
  NavDropdown,
  Card,
  Row,
  Col,
} from "react-bootstrap";
import "./App.css";

class App extends Component {
  state = {
    list: [
      {
        id: 1,
        title: "Ankor Wat",
        subtitle: "In Cambodia",
        img:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRn67_xWCjBN9XIImUR9Cp5H2m556pxnzRlXg&usqp=CAU",
      },
      {
        id: 2,
        title: "Temple",
        subtitle: "In Cambodia",
        img:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqnaH-Ik3i5pa29s7NnWtTuENv1tP5g0jIaw&usqp=CAU",
      },
      {
        id: 3,
        title: "Taprom",
        subtitle: "In Cambodia",
        img:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw33PpQIz965357KK09IEbqx2SOlaVj-uHwQ&usqp=CAU",
      },
      {
        id: 4,
        title: "Mountain",
        subtitle: "In Cambodia",
        img:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxPQq5VfiuixAfER8UKxcpELVcKwkk8RIpKw&usqp=CAU",
      },
    ],
  };
  deleteItem = (id) => {
    this.setState((curr) => ({
      list: curr.list.filter((x) => x.id !== id),
    }));
  };
  renderItem = (item, index) => {
    return (
      <Card key={index} style={{ width: "15rem", margin: "0 10px 10px" }}>
        <Card.Img variant="top" src={item.img} />
        <Card.Body>
          <Card.Title>{item.title}</Card.Title>
          <Card.Text>{item.subtitle}</Card.Text>
          <Button onClick={() => this.deleteItem(item.id)} variant="danger">
            Delete
          </Button>
        </Card.Body>
      </Card>
    );
  };
  render() {
    const { list } = this.state;
    return (
      <>
        <Container fluid className="bg-light">
          <Navbar className="container" bg="light" expand="lg">
            <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#link">Link</Nav.Link>
                <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.2">
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">
                    Something
                  </NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="#action/3.4">
                    Separated link
                  </NavDropdown.Item>
                </NavDropdown>
              </Nav>
              <Form inline>
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                />
                <Button variant="outline-success">Search</Button>
              </Form>
            </Navbar.Collapse>
          </Navbar>
        </Container>
        <Container>
          <Row className="justify-content-center mt-5">
            {list.map(this.renderItem)}
          </Row>
        </Container>
      </>
    );
  }
}

export default App;
